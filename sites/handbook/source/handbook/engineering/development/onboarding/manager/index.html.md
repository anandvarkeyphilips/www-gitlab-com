---
layout: handbook-page-toc
title: Manager Onboarding
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Training

### Iteration Training
Besides normal onboarding training with various tools and processes.  Engineering Managers should also take the [Iteration](https://about.gitlab.com/handbook/product/#iteration) Training.  Engineering Managers can start the process of training by following the steps in this [template](https://gitlab.com/gitlab-com/Product/-/issues/new?issuable_template=iteration-training).  Iteration is a important value of GitLab and not easily learned.  Working with other Engineering Managers and Product Managers during this exercise is encouraged to further enhance the experience.  
