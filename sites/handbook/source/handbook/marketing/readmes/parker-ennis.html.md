---
layout: markdown_page
title: "Parker E.'s README (Sr. PMM)"
---

## Parker Ennis README

Sr. Product Marketing Manager, GitLab CI/CD

## Related pages

* [LinkedIn profile](https://www.linkedin.com/in/parkerennis/)
* [Twitter handle](https://twitter.com/Parker_GitLab)
* [GitLab profile](https://gitlab.com/parker_ennis)

## "User manual"

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTQlxKNcMZTd1TAJFr-V-0dpbBlgwvcsZ1f0OXs34jlp2F8msGqJVlXegRfcp4PfWDjMJXTY8eCeSG1/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

## How I think

![Emergenetics profile](/images/readme_images/parker_emergenetics.png)

## Personality

![HBDI profile](/images/readme_images/parker_HBDI.png)

## Books

* [How to Win Friends and Influence People](https://www.amazon.com/How-Win-Friends-Influence-People/dp/0671027034)
