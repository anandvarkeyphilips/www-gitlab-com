---
layout: handbook-page-toc
title: Support Onboarding
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## New Hire Onboarding in Support

-- Introduction --

When you first join the team everything will be new to you. Don't worry! In order to get you
started with GitLab quickly, apart from the [General Onboarding Checklist](https://gitlab.com/gitlab-com/people-group/employment-templates/-/blob/master/.gitlab/issue_templates/onboarding.md) assigned to you by a People Experience team member on your first day, you will also have a [Support Engineer Onboarding Checklist](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Onboarding%20-%20Support%20Engineer.md) created for you, followed by either a [Support Engineer - Solutions Focus Bootcamp Checklist](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/issue_templates/Onboarding%20-%20Solutions%20Focus.md) or [Support Engineer - GitLab.com Focus Bootcamp Checklist](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/Onboarding%20-%20GitLab-dot-com%20Focus.md)
to help guide you through your training.

## Support Learning Pathways
{: #se-bootcamp}

-- Ongoing training in Support --

## Support Training Project

-- Details --